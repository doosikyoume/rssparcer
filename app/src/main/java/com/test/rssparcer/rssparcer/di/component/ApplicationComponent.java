package com.test.rssparcer.rssparcer.di.component;

import android.content.Context;

import com.test.rssparcer.rssparcer.di.module.ApplicationModule;
import com.test.rssparcer.rssparcer.netapi.ApiInterface;
import com.test.rssparcer.rssparcer.ui.main.RSSInteractor;

import javax.inject.Singleton;

import dagger.Component;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;


/**
 * ApplicationComponent
 */

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    void inject(Context pContext);

    Context getContext();

    Cache getCache();

    OkHttpClient getOkHttpClient();

    Retrofit getRetrofit();

    RealmConfiguration provideRealmConfiguration();

    Realm provideDefaultRealm();

    ApiInterface provideApiInterface();

    RSSInteractor provideRSSInteractor();
}
