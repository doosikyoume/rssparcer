package com.test.rssparcer.rssparcer.di.component;


import com.test.rssparcer.rssparcer.di.PerActivity;
import com.test.rssparcer.rssparcer.di.module.ActivityModule;
import com.test.rssparcer.rssparcer.ui.main.RSSActivity;
import com.test.rssparcer.rssparcer.ui.main.RSSInteractor;

import dagger.Component;

/**
 * ActivityComponent
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    void inject(RSSActivity mainActivity);

    RSSInteractor provideRSSInteractor();

}
