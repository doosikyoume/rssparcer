package com.test.rssparcer.rssparcer.model.dbhelper;

import com.test.rssparcer.rssparcer.C;
import com.test.rssparcer.rssparcer.model.Channel;
import com.test.rssparcer.rssparcer.model.Feed;
import com.test.rssparcer.rssparcer.model.Image;
import com.test.rssparcer.rssparcer.model.Item;
import com.test.rssparcer.rssparcer.utils.log;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.exceptions.RealmException;

/**
 * FeedHelper
 */

public class FeedHelper extends BaseHelper<Feed> {
    private static final String TAG = FeedHelper.class.getSimpleName();

    public FeedHelper(Realm pRealm) {
        super(pRealm, Feed.class);
    }

    @Override
    public void copyToRealmOrUpdateTransaction(Feed pLocalEntity) {
        beginTransaction();
        try {
            pLocalEntity.setId(C.API_ENDPOINT);
            Channel localChannel = pLocalEntity.getChannel();
            RealmList<Item> localItems = localChannel.getAtricles();

            RealmList<Item> realmItems = new RealmList<>();
            if (localItems != null){
                for (Item item : getRealm().copyToRealmOrUpdate(localItems)) {
                    item.setFeedid(C.API_ENDPOINT);
                    realmItems.add(item);
                }
            }
            localChannel.setAtricles(realmItems);

            Image localImage = localChannel.getImage();
            Image realmImage = getRealm().copyToRealmOrUpdate(localImage);
            localChannel.setImage(realmImage);

            Channel realmChannel = getRealm().copyToRealmOrUpdate(localChannel);
            pLocalEntity.setChannel(realmChannel);
            getRealm().copyToRealmOrUpdate(pLocalEntity);

            commitTransaction();
        } catch (RealmException pEx) {
            cancelTransaction();
            log.et(TAG, pEx);
        }
    }

    public Feed getCurrentFeed() {
        return query()
                .equalTo("id", C.API_ENDPOINT)
                .isNotNull("channel")
                .findFirst();
    }



}
