package com.test.rssparcer.rssparcer.model;

/**
 * Item
 */

import android.text.TextUtils;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

@Root(name = "item", strict = false)
public class Item extends RealmObject  {

    @PrimaryKey
    @Element(name = "guid")
    private String guid;

    @Element(name = "title")
    private String title;

    @Element(name = "pdalink", required = false)
    private String pdalink;

    @Element(name = "link", required = false)
    private String link;

    @Element(name = "author", required = false)
    private String author;

    private String feedid;


    @Ignore
    private String worklink;


    public String getGuid() {
        return guid;
    }

    public void setGuid(String pGuid) {
        guid = pGuid;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String pAuthor) {
        author = pAuthor;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPdalink() {
        return pdalink;
    }

    public void setPdalink(String pPdalink) {
        pdalink = pPdalink;
    }

    public String getWorklink() {
        return TextUtils.isEmpty(pdalink) ? link : pdalink;
    }

    public void setWorklink(String pWorklink) {
        worklink = pWorklink;
    }

    public String getFeedid() {
        return feedid;
    }

    public void setFeedid(String pFeedid) {
        feedid = pFeedid;
    }
}