package com.test.rssparcer.rssparcer.model;

/**
 * Item
 */

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


@Root(name = "channel", strict = false)
public class Channel extends RealmObject {

    @PrimaryKey
    @Element(name = "link", required = false)
    private String link;

    @Element(name = "title", required = false)
    private String title;

    @ElementList(name = "item", inline = true)
    private RealmList<Item> atricles;

    @Element(name = "image")
    private Image image;

    public String getLink() {
        return link;
    }

    public void setLink(String pLink) {
        link = pLink;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String pTitle) {
        title = pTitle;
    }

    public RealmList<Item> getAtricles() {
        if (atricles == null){
            return new RealmList<>();
        }
        return atricles;
    }

    public void setAtricles(RealmList<Item> pAtricles) {
        atricles = (pAtricles == null ? new RealmList<>() : pAtricles);
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image pImage) {
        image = pImage;
    }
}