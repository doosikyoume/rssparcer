package com.test.rssparcer.rssparcer.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;

import com.test.rssparcer.rssparcer.di.ActivityContext;

import dagger.Module;
import dagger.Provides;

/**
 * ActivityModule
 */

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    LayoutInflater provideLayoutInflater() {return mActivity.getLayoutInflater();}
}
