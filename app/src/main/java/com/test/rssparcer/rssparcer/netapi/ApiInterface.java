package com.test.rssparcer.rssparcer.netapi;

import com.test.rssparcer.rssparcer.model.Feed;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * ApiInterface
 * @see <a href="https://documenter.getpostman.com/view/875693/rss/77h6i4x">Postman test</a>
 */

public interface ApiInterface {

    @GET("rus/rss")
    Observable<Feed> getArticles();
}
