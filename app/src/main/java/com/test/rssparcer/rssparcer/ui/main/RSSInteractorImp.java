package com.test.rssparcer.rssparcer.ui.main;

import com.test.rssparcer.rssparcer.model.Feed;
import com.test.rssparcer.rssparcer.model.Item;
import com.test.rssparcer.rssparcer.model.dbhelper.FeedHelper;
import com.test.rssparcer.rssparcer.model.dbhelper.ItemHelper;
import com.test.rssparcer.rssparcer.netapi.ApiInterface;
import com.test.rssparcer.rssparcer.utils.log;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.realm.OrderedRealmCollection;
import io.realm.Realm;

/**
 * RSSInteractorImp
 */

public class RSSInteractorImp implements RSSInteractor{


    private static final String TAG = RSSInteractorImp.class.getSimpleName();


    private ApiInterface mApiService;

    private Realm mRealm;
    private FeedHelper mFeedHelper;
    private ItemHelper mItemHelper;

    @Inject
    public RSSInteractorImp(ApiInterface pApiService, Realm pRealm) {
        mApiService = pApiService;
        mRealm = pRealm;
        mFeedHelper = new FeedHelper(mRealm);
        mItemHelper = new ItemHelper(mRealm);
    }


    public RSSInteractorImp(ApiInterface pApiService, FeedHelper pHelper) {
        mApiService = pApiService;
        mFeedHelper = pHelper;
    }


    @Override
    public void updateArticles() {
        updateArticles(null);
    }

    @Override
    public void updateArticles(OnUpdateFeed pOnUpdateFeed) {
        mApiService.getArticles()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        x -> saveFeed(x, pOnUpdateFeed),
                        t-> showError(t, pOnUpdateFeed));
    }

    private void showError(Throwable pLocalizedMessage, OnUpdateFeed pOnUpdateFeed) {
        if (pOnUpdateFeed != null){
            pOnUpdateFeed.onError(pLocalizedMessage);
        }
        log.et(TAG, pLocalizedMessage);
    }

    private void saveFeed(Feed pX, OnUpdateFeed pOnUpdateFeed) {

        mFeedHelper.copyToRealmOrUpdateTransaction(pX);
        if (pOnUpdateFeed != null) {
            pOnUpdateFeed.onSuccess();
        }
    }

    public Realm getRealm() {
        return mRealm;
    }

    @Override
    public OrderedRealmCollection<Item> getArticles() {
        return mItemHelper.findAll();
    }
}
