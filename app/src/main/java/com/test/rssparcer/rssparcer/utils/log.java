package com.test.rssparcer.rssparcer.utils;

import android.util.Log;


/**
 * log
 */

public class log {
    private static final String TAG = "RSSParser";

    public static void log(final int priority, final String tag, final String msg, final Throwable tr) {
        String message;
        message = tr == null ? msg : msg + "\n" + Log.getStackTraceString(tr);
        Log.println(priority, tag, message);
    }


// ================== TAG logging ===================== //

    public static void vt(String tag, String format, Object... args) {
        log(Log.VERBOSE, tag, String.format(format, args), null);
    }

    public static void dt(String tag, String format, Object... args) {
        log(Log.DEBUG, tag, String.format(format, args), null);
    }

    public static void it(String tag, String format, Object... args) {
        log(Log.INFO, tag, String.format(format, args), null);
    }


    public static void wt(String tag, Throwable tr) {
        log(Log.WARN, tag, null, tr);
    }

    public static void wt(String tag, String msg) {
        log(Log.WARN, tag, msg, null);
    }

    public static void wt(String tag, String msg, Throwable tr) {
        log(Log.WARN, tag, msg, tr);
    }


    public static void et(String tag, Throwable tr) {
        log(Log.ERROR, tag, null, tr);
    }

    public static void et(String tag, String msg) {
        log(Log.ERROR, tag, msg, null);
    }

    public static void et(String tag, String msg, Throwable tr) {
        log(Log.ERROR, tag, msg, tr);
    }


// ================== Not TAG logging ===================== //

    public static void v(String format, Object... args) {
        log(Log.VERBOSE, TAG, String.format(format, args), null);
    }

    public static void d(String format, Object... args) {
        log(Log.DEBUG, TAG, String.format(format, args), null);
    }


    public static void i(String format, Object... args) {
        log(Log.INFO, TAG, String.format(format, args), null);
    }


    public static void w(String format, Object... args) {
        log(Log.WARN, TAG, String.format(format, args), null);
    }

    public static void w(Throwable tr) {
        log(Log.WARN, TAG, null, tr);
    }

    public static void e(String format, Object... args) {
        log(Log.ERROR, TAG, String.format(format, args), null);
    }






}