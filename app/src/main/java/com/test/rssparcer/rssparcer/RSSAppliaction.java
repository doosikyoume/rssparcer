package com.test.rssparcer.rssparcer;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.test.rssparcer.rssparcer.di.component.ApplicationComponent;
import com.test.rssparcer.rssparcer.di.component.DaggerApplicationComponent;
import com.test.rssparcer.rssparcer.di.module.ApplicationModule;

import io.fabric.sdk.android.Fabric;
import javax.inject.Inject;

import io.realm.Realm;

/**
 * RSSAppliaction
 */

public class RSSAppliaction extends Application {
    private ApplicationComponent mAppComponent;

    public ApplicationComponent getAppComponent() {
        return mAppComponent;
    }

    @Inject
    Realm mRealm;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mAppComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        mAppComponent.inject(this);
    }



    @Override
    public void onTerminate() {
        if (mRealm != null) {
            mRealm.close();
        }
        super.onTerminate();
    }
}
