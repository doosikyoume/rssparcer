package com.test.rssparcer.rssparcer.model.migration;

import io.realm.DynamicRealm;
import io.realm.RealmMigration;
import io.realm.RealmSchema;

/**
 * Migration
 */

public class Migration implements RealmMigration {
    private static String TAG = Migration.class.getSimpleName();
    public static int VERSION = 2;


    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        for (long i = oldVersion + 1; i <= newVersion; i++) {
            versionChange((int) i, realm);
        }
    }

    private void versionChange(int pVersion, final DynamicRealm pRealm) {
        final RealmSchema schema = pRealm.getSchema();
        switch (pVersion) {

            case 2:
                schema.get("Item")
                        .addField("feedid", String.class);
                break;
        }

    }
}
