package com.test.rssparcer.rssparcer.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Image
 */
@Root(name = "image", strict = false)
public class Image extends RealmObject {
    @PrimaryKey
    @Element(name = "url")
    private String url;

    @Element(name = "link", required = false)
    private String link;

    @Element(name = "title", required = false)
    private String title;

    public String getUrl() {
        return url;
    }

    public void setUrl(String pUrl) {
        url = pUrl;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String pLink) {
        link = pLink;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String pTitle) {
        title = pTitle;
    }
}
