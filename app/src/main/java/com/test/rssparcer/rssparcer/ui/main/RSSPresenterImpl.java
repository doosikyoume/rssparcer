package com.test.rssparcer.rssparcer.ui.main;

import com.test.rssparcer.rssparcer.model.Item;

import io.realm.OrderedRealmCollection;

/**
 * RSSPresenterImpl
 */

public class RSSPresenterImpl implements RSSPresenter, RSSInteractor.OnUpdateFeed {



    private RSSView mRSSView;
    private RSSInteractorImp mInteractor;


    public RSSPresenterImpl(RSSView pRSSView, RSSInteractorImp pInteractor) {
        mRSSView = pRSSView;
        mInteractor = pInteractor;
    }

    @Override
    public void onDestroy() {
        mInteractor = null;
        mRSSView = null;
    }

    @Override
    public void updateFeed() {
        mRSSView.showProgress();
        mInteractor.updateArticles(this);
    }

    @Override
    public OrderedRealmCollection<Item> getArticles() {
        return mInteractor.getArticles();
    }

    @Override
    public void onError(Throwable pError) {
        if (mRSSView != null){
            mRSSView.showError(pError);
        }
    }

    @Override
    public void onSuccess() {
        if (mRSSView != null){
            mRSSView.hideProgress();
        }
    }
}
