package com.test.rssparcer.rssparcer.ui.main;

import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.test.rssparcer.rssparcer.R;
import com.test.rssparcer.rssparcer.model.Item;
import com.test.rssparcer.rssparcer.ui.news.NewsActivity;
import com.test.rssparcer.rssparcer.utils.ui.adapters.BaseEntityAdapter;

import io.realm.OrderedRealmCollection;

/**
 * CRSSAdapter
 */

public class RSSAdapter extends BaseEntityAdapter<Item> {

    private final Context mContext;
    private final OnEmpty mOnEmpty;

    public RSSAdapter(Context pContext, @Nullable OrderedRealmCollection pData, OnEmpty pOnEmpty) {
        super(pContext, pData, R.layout.item_rss);
        mContext = pContext;
        mOnEmpty = pOnEmpty;
    }


    @Override
    protected void onBind(Item pEntity, BaseEntityHolder pEntityHolder) {
        pEntityHolder.findView(R.id.tv_feed_title, TextView.class).setText(pEntity.getTitle());
        pEntityHolder.findView(R.id.tv_feed_url, TextView.class).setText(pEntity.getWorklink());
        pEntityHolder.setOnClick(R.id.cv_feed);
    }


    @Override
    protected void onEmptyChange(boolean pIsEmpty) {
        if (mOnEmpty != null){
            mOnEmpty.onEmpty(pIsEmpty);
        }
    }

    @Override
    protected void onClick(int pResourceId, Item pEntity) {
        switch (pResourceId){
            case R.id.cv_feed:
                NewsActivity.start(mContext, pEntity.getWorklink());
                break;
        }
    }

    public interface OnEmpty{
        void onEmpty(boolean pIsEmpty);
    }
}
