package com.test.rssparcer.rssparcer.model.dbhelper;

import com.test.rssparcer.rssparcer.C;
import com.test.rssparcer.rssparcer.model.Item;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;

/**
 * ItemHelper
 */

public class ItemHelper extends BaseHelper {
    public ItemHelper(Realm pRealm) {
        super(pRealm, Item.class);
    }

    @Override
    public OrderedRealmCollection<Item> findAll() {
        return query().equalTo("feedid", C.API_ENDPOINT).findAll();
    }
}
