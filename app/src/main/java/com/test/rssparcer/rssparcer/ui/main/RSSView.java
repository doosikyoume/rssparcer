package com.test.rssparcer.rssparcer.ui.main;

/**
 * RSSView
 */

public interface RSSView {
    void showError(Throwable pThrowable);
    void showProgress();
    void hideProgress();
    void onEmptyList();
    void onNonEmptyList();
}
