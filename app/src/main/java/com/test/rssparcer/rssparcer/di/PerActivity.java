package com.test.rssparcer.rssparcer.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * PerActivity
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerActivity {
}

