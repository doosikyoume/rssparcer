package com.test.rssparcer.rssparcer.ui.main;

import com.test.rssparcer.rssparcer.model.Item;

import io.realm.OrderedRealmCollection;

/**
 * RSSPresenter
 */

public interface RSSPresenter {
    void onDestroy();
    void updateFeed();
    OrderedRealmCollection<Item> getArticles();
}
