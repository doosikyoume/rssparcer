package com.test.rssparcer.rssparcer.ui.main;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.test.rssparcer.rssparcer.R;
import com.test.rssparcer.rssparcer.RSSAppliaction;
import com.test.rssparcer.rssparcer.di.component.ActivityComponent;
import com.test.rssparcer.rssparcer.di.component.DaggerActivityComponent;
import com.test.rssparcer.rssparcer.di.module.ActivityModule;

import javax.inject.Inject;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class RSSActivity extends AppCompatActivity implements RSSView, RSSAdapter.OnEmpty {


    private static final String TAG = RSSActivity.class.getSimpleName();


    @Inject
    RSSInteractorImp mInteractor;


    private RecyclerView mListView;
    private RSSAdapter mAdapter;
    private ActivityComponent mActivityComponent;
    private RSSPresenterImpl mPresenter;
    private ProgressDialog mProgressDialog;
    private TextView mEmptyView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initProgress();

        mActivityComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(((RSSAppliaction) getApplicationContext()).getAppComponent())
                .build();
        mActivityComponent.inject(this);

        mPresenter = new RSSPresenterImpl(this, mInteractor);
        mPresenter.updateFeed();

        mListView = (RecyclerView) findViewById(R.id.lv_feeds);
        mEmptyView = (TextView) findViewById(R.id.tv_empty_feed);
        mListView.setLayoutManager(new LinearLayoutManager(this));
        //mListView.setEmptyView(findViewById(R.id.tv_empty));
        mAdapter = new RSSAdapter(this, mPresenter.getArticles(), this);
        mListView.setAdapter(mAdapter);

    }

    private void initProgress() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle("Loading");
        mProgressDialog.setMessage("Wait while loading Feed");
        mProgressDialog.setCancelable(false);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_feed, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.m_update:  mPresenter.updateFeed(); break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showError(Throwable pThrowable) {
        Crouton.makeText(this, pThrowable.getLocalizedMessage(), Style.ALERT).show();
        hideProgress();
    }

    @Override
    public void showProgress() {
        if (!mProgressDialog.isShowing()){
            mProgressDialog.show();
        }
    }

    @Override
    public void hideProgress() {
        if (mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onEmptyList() {

    }

    @Override
    public void onNonEmptyList() {

    }

    @Override
    public void onDestroy() {
        mPresenter.onDestroy();
        super.onDestroy();

    }

    @Override
    public void onEmpty(boolean pIsEmpty) {
        mEmptyView.setVisibility(pIsEmpty ? View.VISIBLE : View.GONE);
    }
}
