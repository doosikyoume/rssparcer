package com.test.rssparcer.rssparcer.ui.main;

import com.test.rssparcer.rssparcer.model.Item;

import io.realm.OrderedRealmCollection;

/**
 * RSSInteractor
 */

public interface RSSInteractor {
    void updateArticles();
    void updateArticles(OnUpdateFeed pOnUpdateFeed);
    OrderedRealmCollection<Item> getArticles();

    interface OnUpdateFeed {
        void onError(Throwable pLocalizedMessage);
        void onSuccess();
    }
}
