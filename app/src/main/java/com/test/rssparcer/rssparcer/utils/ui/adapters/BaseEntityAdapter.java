package com.test.rssparcer.rssparcer.utils.ui.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.rssparcer.rssparcer.R;

import io.realm.OrderedRealmCollection;
import io.realm.RealmObject;
import io.realm.RealmRecyclerViewAdapter;

/**
 * BaseEntityAdapter
 */

public abstract class BaseEntityAdapter<T extends RealmObject> extends RealmRecyclerViewAdapter implements View.OnClickListener {


    private final Context mContext;
    @Nullable
    private final OrderedRealmCollection mData;
    private boolean mIsEmpty;
    private LayoutInflater mInflator;

    private final int mResourceId;

    public BaseEntityAdapter(Context pContext, @Nullable OrderedRealmCollection pData, int pResourceId) {
        super(pData, true);
        mContext = pContext;
        mData = pData;
        mInflator = LayoutInflater.from(mContext);
        mResourceId = pResourceId;
        mIsEmpty = true;
    }

    @Override
    public BaseEntityHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BaseEntityHolder(mInflator.inflate(mResourceId, parent, false));
    }


    @Override
    public int getItemCount() {
        int count = super.getItemCount();
        boolean emptyState = (count == 0);
        if (emptyState != mIsEmpty) {
            onEmptyChange(emptyState);
            mIsEmpty = emptyState;
        }
        return count;
    }

    @Nullable
    @Override
    public T getItem(int index) {
        return (T) super.getItem(index);
    }

    protected abstract void onBind(T pEntity, BaseEntityHolder pEntityHolder);

    @Override
    public final void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        T item = getItem(position);
        BaseEntityHolder entityHolder = (BaseEntityHolder) holder;
        entityHolder.setEntity(item);
        onBind(item, entityHolder);
    }


    @Override
    public final void onClick(View v) {
        Object item = v.getTag(R.id.id_tag_entity);
        if (item == null) {
            return;
        }
        onClick(v.getId(), (T) item);
    }

    protected void onClick(int pResourceId, T pEntity){
        /*Override*/
    }

    protected void onEmptyChange(boolean pIsEmpty){
        /*Override*/
    }


    public class BaseEntityHolder extends RecyclerView.ViewHolder {
        private View mItemView;
        private T mEntity;

        public BaseEntityHolder(View pItemView) {
            super(pItemView);
            mItemView = pItemView;
        }

        public View findView(int pResId) {
            return mItemView.findViewById(pResId);
        }

        public <V extends View> V findView(int pResId, Class<V> pClass) {
            return pClass.cast(mItemView.findViewById(pResId));
        }

        public void setOnClick(int pResId) {
            View v = findView(pResId);
            v.setTag(R.id.id_tag_entity, getEntity());
            v.setOnClickListener(BaseEntityAdapter.this);
        }

        public void setEntity(T pEntity) {
            mEntity = pEntity;
        }

        public T getEntity() {
            return mEntity;
        }
    }


}
