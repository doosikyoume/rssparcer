package com.test.rssparcer.rssparcer.ui.news;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import com.test.rssparcer.rssparcer.R;

public class NewsActivity extends AppCompatActivity {

    private static final String B_URL = "B_URL";

    public static void start(Context pContext, String pUrl) {
        Intent args = new Intent();
        args.setClass(pContext, NewsActivity.class);
        args.putExtra(B_URL, pUrl);
        pContext.startActivity(args);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        String url = getIntent().getStringExtra(B_URL);
        ((WebView)findViewById(R.id.wv_newshtml)).loadUrl(url);
    }

}
