package com.test.rssparcer.rssparcer.model.dbhelper;

import android.support.annotation.Nullable;

import com.test.rssparcer.rssparcer.utils.log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.exceptions.RealmException;

/**
 * @author doosik
 * Базовый класс работы с базой Realm на случай если сущностей больше чем одна
 */
public abstract class BaseHelper<T extends RealmObject> {
    private static final String TAG = BaseHelper.class.getSimpleName();
    protected final Realm mRealm;
    protected final Class<T> mClazz;

    public BaseHelper(Realm pRealm, Class<T> pClazz) {
        mRealm = pRealm;
        mClazz = pClazz;
    }

    public Realm getRealm() {
        return mRealm;
    }

    /**
     * Устанавливает локальную id, если сущность была создана на устройстве
     * @param pEntity Сущность
     */
    protected void setInternalId(T pEntity) {
        //Override this is needed.
    }

    public void beginTransaction() {
        if (mRealm.isInTransaction()){
            cancelTransaction();
        }
        mRealm.beginTransaction();
    }

    public void commitTransaction() {
        mRealm.commitTransaction();
    }



    public void cancelTransaction() {
        mRealm.cancelTransaction();
    }




    /**
     * Возвращает сущность из базы по СЕРВЕРНОМУ ID
     * @param entityId Серверный ID
     * @return сущность
     */
    @Nullable
    public T getEntityById(long entityId) {
        return query().equalTo("id", entityId).findFirst();
    }


    public T createLocal(long entityId) {
        return createLocal(getEntityById(entityId));
    }

    public T createLocal(T pEntity) {
        return getRealm().copyFromRealm(pEntity);
    }

    public List<T> createLocal(RealmResults<T> pEntityList) {
        return getRealm().copyFromRealm(pEntityList);
    }


    /**
     * Возвращает сущность из базы по СЕРВЕРНОМУ ID
     * @param pId Серверный ID
     * @return сущность
     */
    @Nullable
    public T getEntityById(String pId) {
        return query().equalTo("id", pId).findFirst();
    }

    public long count() {
        return query().count();
    }

    /**
     * Create entity and set internal identificator
     */
    public T create() {

        T entity = mRealm.createObject(mClazz);
        setInternalId(entity);
        return entity;
    }

    protected T createOrUpdate(long pId) {

        T entity = pId != 0 ? getEntityById(pId) : create();
        if (entity == null) {
            entity = create();

        }
        return entity;
    }

    protected T createOrUpdate(String pId) {
        T entity = getEntityById(pId);
        if (entity == null) {
            entity = create();

        }
        return entity;
    }


    protected void copyToRealm(T pEntity){
        beginTransaction();
        try {
            getRealm().copyToRealmOrUpdate(pEntity);
            commitTransaction();
        } catch (RealmException pEx) {
            cancelTransaction();
            log.et(TAG, pEx);
        }
    }

    public void delete(T pEntity) {
        pEntity.deleteFromRealm();
    }

    public void deleteWithTransaction(T pEntity) {
        beginTransaction();
        try {
            pEntity.deleteFromRealm();
            commitTransaction();
        } catch (RealmException pEx) {
            cancelTransaction();
            log.e(TAG, pEx);
        }
    }

    public void delete(List<T> pEntitys) {
        final Object[] pEntityArray = pEntitys.toArray();
        delete(pEntityArray);
    }

    public void delete(Object[] pEntityArray) {
        for (Object anAll : pEntityArray) {
            T item = (T) anAll;
            item.deleteFromRealm();
        }
    }

    public void deleteAll() {
        mRealm.where(mClazz).findAll().deleteAllFromRealm();
    }

    public OrderedRealmCollection<T> findAll() {
        return mRealm.where(mClazz).findAll();
    }

    public RealmQuery<T> query() {
        return mRealm.where(mClazz);
    }

    public T createOrUpdateFromJson(JSONObject pJson) throws RealmException, JSONException, ParseException {
        return mRealm.createOrUpdateObjectFromJson(mClazz, pJson);
    }

    public List<T> createOrUpdateAllFromJson(JSONArray pJson) throws JSONException, RealmException, ParseException {
        final int length = pJson.length();
        List<T> result = new ArrayList<>(length);

        for (int i = 0; i < length; i++) {
            T entity = createOrUpdateFromJson(pJson.getJSONObject(i));
            if (entity != null) {
                result.add(entity);
            }
        }

        return result;
    }

    public void createOrUpdateAllFromJsonTransaction(JSONArray pJson) throws JSONException, RealmException {
        try {
            beginTransaction();
            createOrUpdateAllFromJson(pJson);
            commitTransaction();
        } catch (RealmException pE) {
            cancelTransaction();


            log.et(TAG, pE.getLocalizedMessage());
        } catch (ParseException pE) {
            cancelTransaction();
            log.et(TAG, pE.getLocalizedMessage());
        }

    }

    public void copyToRealmOrUpdateTransaction(T pEntity){
        beginTransaction();
        try {
            getRealm().copyToRealmOrUpdate(pEntity);
            log.dt(TAG, "copyToRealm: %s", pEntity.getClass().getSimpleName());
            commitTransaction();
        } catch (RealmException pEx) {
            cancelTransaction();
            log.et(TAG, pEx);
        }

    };
}


