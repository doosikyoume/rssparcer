package com.test.rssparcer.rssparcer.di.module;

import android.content.Context;

import com.test.rssparcer.rssparcer.C;
import com.test.rssparcer.rssparcer.model.migration.Migration;
import com.test.rssparcer.rssparcer.netapi.ApiInterface;
import com.test.rssparcer.rssparcer.ui.main.RSSInteractor;
import com.test.rssparcer.rssparcer.ui.main.RSSInteractorImp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.SimpleXmlConverterFactory;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

/**
 * ApplicationModule
 */

@Module
public class ApplicationModule {

    private final Context mContext;

    public ApplicationModule(Context context) {
        mContext = context;
    }

    @Provides
    Context provideContext() {
        return mContext;
    }


    @Provides
    @Singleton
    RealmConfiguration provideRealmConfiguration() {
        Realm.init(mContext);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .schemaVersion(Migration.VERSION)
                .migration(new Migration())
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
        return realmConfiguration;
    }

    @Provides
    @Singleton
    Realm provideDefaultRealm(RealmConfiguration config) {
        return Realm.getInstance(config);
    }

    @Provides
    @Singleton
    Cache provideHttpCache(Context pContext) {
        int cacheSize = 8 * 1024 * 1024;
        return new Cache(pContext.getCacheDir(), cacheSize);
    }



    @Provides
    @Singleton
    OkHttpClient provideOkhttpClient(Cache cache) {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.cache(cache);
        return client.build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(C.API_ENDPOINT)
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }


    @Provides
    @Singleton
    ApiInterface provideApiInterface(Retrofit mRetrofit){
        return mRetrofit.create(ApiInterface.class);
    }


    @Provides
    RSSInteractor provideRSSInteractor(ApiInterface pInterface, Realm mRealm){
        return new RSSInteractorImp(pInterface, mRealm);
    }

}
