package com.test.rssparcer.rssparcer.model;

/**
 * Feed
 */

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

@Root(name = "rss", strict = false)
public class Feed extends RealmObject {

    @PrimaryKey
    private String id;

    @Element(name = "channel")
    private Channel channel;

    public String getId() {
        return id;
    }

    public void setId(String pId) {
        id = pId;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel pChannel) {
        channel = pChannel;
    }
}