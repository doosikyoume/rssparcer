package com.test.rssparcer.rssparcer;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.test.rssparcer.rssparcer.di.component.ApplicationComponent;
import com.test.rssparcer.rssparcer.di.component.DaggerApplicationComponent;
import com.test.rssparcer.rssparcer.di.module.ApplicationModule;
import com.test.rssparcer.rssparcer.model.Feed;
import com.test.rssparcer.rssparcer.netapi.ApiInterface;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import io.reactivex.Observable;
import io.reactivex.subscribers.TestSubscriber;
import retrofit2.Retrofit;

import static org.junit.Assert.assertNotNull;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class MainInstrumentedTest {


    private ApplicationComponent mConponent;

    @Before
    public void setUp() throws IOException {

        Context applicationContext = InstrumentationRegistry.getContext();
        mConponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(applicationContext))
                .build();
        mConponent.inject(applicationContext);


    }

    @Test
    public void rssRetrofit() throws Exception {
        Retrofit retrofit = mConponent.getRetrofit();
        assertNotNull(retrofit);
    }

    @Test
    public void testApiService(){
        ApiInterface service = mConponent.provideApiInterface();
        assertNotNull(service);
        Observable<Feed> feedObserver = service.getArticles();
        TestSubscriber<Feed> testSubscriber = new TestSubscriber<>();
        feedObserver.test().onSubscribe(testSubscriber);
        testSubscriber.assertNoErrors();
        testSubscriber.assertEmpty();
    }


}
